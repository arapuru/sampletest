/**
 * 
 */
package com.example.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Aravind
 *
 */
@RestController
public class ConsumerAPIController {

	
	@Autowired 
	RestTemplate restTemplate;
	
	@GetMapping("/users/{userid}")
	public ResponseEntity<String> getUserEmailId(@PathVariable("userid") Long userId) {
		
		ResponseEntity<UserResponse> userResponseDTO = null;
		userResponseDTO = restTemplate.getForEntity("https://reqres.in/api/users/"+userId, UserResponse.class);
		
		return new ResponseEntity<String>(userResponseDTO.getBody().getData().getEmail(), HttpStatus.OK);
	}

}

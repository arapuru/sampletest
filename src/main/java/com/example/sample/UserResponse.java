/**
 * 
 */
package com.example.sample;

/**
 * @author Aravind
 *
 */
public class UserResponse {
	private UserDTO data;
	private Support support;
	
	public UserDTO getData() {
		return data;
	}
	public void setData(UserDTO data) {
		this.data = data;
	}
	public Support getSupport() {
		return support;
	}
	public void setSupport(Support support) {
		this.support = support;
	}

}
